#!/usr/bin/env python
# encoding: utf-8
"""
untitled.py

Created by William Gelhar on 2010-12-07.
Copyright (c) 2010 ButtsCo.. All rights reserved.
"""

import sys
import os
import Image, ImageDraw
import random
from time import sleep

try:
    import psyco
    psyco.full()
except ImportError:
    pass


#global settings
BLOCKSIZE = 16
MODIFIER = 4
REFINES = 5
IMAGEWIDTH = 256* (2 ^ MODIFIER)
IMAGEHEIGHT = 128 * (2 ^ MODIFIER)

if not IMAGEWIDTH%BLOCKSIZE == 0: raise AssertionError

XBLOCKS = 16
YBLOCKS = 8
LANDP = 0.50

DEPTH = 20
DEPTHM = (((DEPTH * 2)+1)**2 - 1)/8

landcolour = "green"
watercolour = "lightblue"

heightcolours = [ watercolour, #0 watercolour
				landcolour,	#1: landcolour
				"green",	#2
				"#198D19",	#3
				"#309830", 	#4
				"#45A245", 	#5
				"#58AB58",	#6
				"#69B369", 	#7
				"#78BB78",	#8
				"#AED6AE"]	#9

im = Image.new('RGBA',(IMAGEWIDTH,IMAGEHEIGHT))
draw = ImageDraw.Draw(im)

landarr = [] #the array that holds the entire map

subdiv = 1

#Initializes the world by just applying two-tone noise to it through 'random'
def initworld():	
	global landarr	
	if landarr != []: raise AssertionError	
	for y in range(YBLOCKS):
		landarr.append([])
		for x in range(XBLOCKS):
			if random.random() > LANDP:
				landarr[y].append(1)
			else:
				landarr[y].append(0)
								
#Refines the resolution of the world by simply repeating every block in to four blocks.
#	It seems like there would be an easier way just with python natively but I couldn't find any,
#	this seemed to be the best way of going about it.
def subdivide(arr):
	newarr = []
	for y in range(len(arr)):
		newarrline = []
		for x in range(len(arr[y])):
			newarrline.append(arr[y][x])
			newarrline.append(arr[y][x])
		newarr.append(newarrline)
		newarr.append(newarrline)			
	return newarr

#Refines the terrain using the functions 'makesurmat' and 'chooseblock'.
#Come to think of it, it really shouldn't be returning sur, or at least it should be 
#returing another array that it modifies instead of modifying landarr and returning 'sur'.			
def refineterrain():
	global landarr
	sur = makesurmat(landarr, 1) #calculates the map of how surrounded each tile is by land
	for y in range(len(landarr)):
		for x in range(len(landarr[y])):
			#randomly (but structured) determines whether this tile should be a land tile
			#or a water tile, refining the land distribution for the higher resolution map.
			landarr[y][x] = chooseblock(y,x,sur[y][x]) 		
	return sur
			
#Calculates the number of surrounding land tiles for each tile of the map and returns them
#in an array structured the same as 'landarr'.		
#'arr' is the array to be processed, 'depth' is just the kernel dimension for the convolution.			
def makesurmat(arr, depth):
	surmat = []
	for y in range(len(arr)):
		surmat.append([])
		for x in range(len(arr[y])):
			surroundn = 0
			for i in range(-depth,depth + 1):
				for j in range(-depth,depth + 1):
					#A whole buunch of conditions to make sure we're not exceeding a bound
					if (((i + y) < 0) or ((j + x) < 0)) \
					or ((i == j) and (i ==0)) \
					or ((i + y) > len(arr) - 1) \
					or  ((j + x) > len(arr[y]) - 1):
						pass
					#and just one condition if we can check for land  and land exists
					elif arr[y + i][x + j] == 1:
						surroundn += 1
			surmat[y].append(surroundn)
	return surmat
	
#Here the map is refined to 'add resolution' to it and carve out some more land.
#Uses the number of surrounding land tiles for each tile to affect the probability of
#the land tile becoming a land tile or water tile. The more land that is around the tile
#the higher the probability of it being land.
def chooseblock(y,x, sur):	
	sample = random.random()
	if sur > 2 and sur < 4:
		if sample  > (LANDP / 5) * 7 :
			return 1
		else:
			return 0
	elif sur > 4 and sur < 7:
		if sample  > (LANDP / 4) * 3 :
			return 1
		else:
			return 0
	elif sur > 5 and sur < 8:
		if sample  >(LANDP / 5) * 1:
			return 1
		else:
			return 0
	elif sur > 7:
		if sample  > 0:
			return 1
		else:
			return 0
	else:
		if sample > LANDP * 2:
			return 1
		else:
			return 0
		
#Just converts the map from the internal representation to an image for display and output.		
def makeimage():	
	global landarr
	print(len(landarr))
	print(len(landarr[0]))	
	imagew = IMAGEWIDTH / len(landarr[0])
	imageh = IMAGEHEIGHT / len(landarr)		
	for y in range(len(landarr)):
		if len(landarr[y]) != len(landarr[0]): raise AssertionError
		for x in range(len(landarr[y])):
			draw.rectangle(
			[(x * imagew,y * imageh),
			((x * imagew)+imagew,(y * imageh) + imageh)], \
			outline=heightcolours[landarr[y][x]], fill=heightcolours[landarr[y][x]])
			
			
#Older, simpler function for making the land have height.			
def makeheight(arr):
	global DEPTH
	surm = makesurmat(arr, DEPTH)	
	for y in range(len(arr)):
		for x in range(len(arr[y])):
			if arr[y][x] == 0:
				pass
			else:
				sample = random.random()		
				height = int(max(1,min(round(sample *2/3 + float(surm[y][x] / DEPTHM)),9)))
				arr[y][x] = height		
	return arr
	
#The more refined function to calculate the height of each land tile, bases it off a 
#function of how far inland the specific tile is, both horizontally and vertically.
def makeheight2(arr):
	for y in arr:
		linecoords = []
		start = -1
		n = 0
		for x in xrange(len(y)):
			#here we go through a row or column and are counting how far we can go from 
			#starting at water to hitting another piece of water, '0' is the representation 
			#for water internally.
			if y[x] != 0:
				if start == -1:
					start = x
					pairl = [x]
					linecoords.append(pairl)
			if y[x] == 0 and start != -1:
					linecoords[n].append(x-1)
					n += 1
					start = -1
		
		if  linecoords != [] and len(linecoords[max(0,len(linecoords) - 1)]) == 1:
			linecoords[len(linecoords) - 1].append(len(y)-1)
		for x in linecoords:
			middle = (x[1] - x[0]) / 2
			for q in xrange(x[0],x[1]+1):
				#seems confusing but all the maxes and mins are just to keep the height between a range
				y[q] = int(max(1,min(9,min(abs(x[1] - q),abs(x[0] - q))* (float(len(heightcolours)-1)) / max(1.0,float(middle)))))
				
				
	for x in xrange(len(arr[0])):
		linecoords = []
		start = -1
		n = 0
		for y in xrange(len(arr)):
			#here we go through a row or column and are counting how far we can go from 
			#starting at water to hitting another piece of water, '0' is the representation 
			#for water internally.
			if arr[y][x] != 0:
				if start == -1:
					start = y
					pairl = [y]
					linecoords.append(pairl)
			if arr[y][x] == 0 and start != -1:
					linecoords[n].append(y-1)
					n += 1
					start = -1
		if linecoords != [] and len(linecoords[max(0,len(linecoords) - 1)]) == 1:
			linecoords[len(linecoords) - 1].append(len(arr)-1)
			
		for j in linecoords:
			#print(linecoords)
			middle = (j[1] - j[0]) / 2
			for q in xrange(j[0],j[1]+1):
				#print y
				#print q

				#seems confusing but all the maxes and mins are just to keep the height between a range
				arr[q][x] = (arr[q][x] + int(max(1,min(9,min(abs(j[1] - q),abs(j[0] - q))* ((float(len(heightcolours)-1)) / max(1.0,float(middle))))))) / 2				
	
	#this is just to randomly perturb the height a bit after the fact, to make it more interesting			
	for y in arr:
		for x in xrange(len(y)):
			if y[x] != 0:
				sample = random.random()
				if sample > 0.6:
					y[x] = max(1,y[x] - 1)
				sample = random.random()
				if sample > 0.6:
					y[x] = min(9,y[x] + 1)
				
	return arr
			

def main():
	global landarr #the array that holds the entire map
	surmatr = []
	initworld()
	makeimage()
	im.show()
	#im.save("start.png")
	for i in range(REFINES):
		landarr = subdivide(landarr)
		refineterrain()
		makeimage()
		im.show()
	#	im.save("start"+str(i)+".png")
	landarr = makeheight2(landarr)
	makeimage()
	im.show()
	#im.save("final.png")

			


if __name__ == '__main__':
	main()

