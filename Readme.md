#Random terrain generation code

![](https://bitbucket.org/williamg/random-terrain-code/raw/190c734a3fa9/stfinalthumb.png)

##What it is

Just a small quick python script I wrote to try out some terrain generation.

##What it does

Starts off with a simple two colour(one for land, one for water I guess) broad image of random noise. Then the resolution is doubled and each pixel is randomly (with a bias according to the surrounding landscape, primarily how many land blocks are surrounding it) perturbed. The algorithm basically refines a block in to a smaller landscape randomly. The final step is to create 'height' (lighter coloured pixels) based off of how far in land vertically and horizontally each pixel is (takes a function of how many pixels are on either side until a water pixel is reached).